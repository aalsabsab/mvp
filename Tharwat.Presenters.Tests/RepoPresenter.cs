﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tharwat.DataAccess;
using Tharwat.Presenters;
using Tharwat.Views;
using Tharwat.Views.Models;

namespace Tharwat.Presenters.Tests
{
    public class RepoPresenter : BaseRepoPresenter<IListView<BaseDto>, BaseDto, TestModel>
    {
        public RepoPresenter(IListView<BaseDto> view, IRepositoryAsync<TestModel> store, IMapper mapper) : base(view, store, mapper)
        {
        }
    }
}
