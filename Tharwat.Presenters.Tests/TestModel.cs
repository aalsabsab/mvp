﻿namespace Tharwat.Presenters.Tests
{
    public class TestModel
    {
        public decimal Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }
}