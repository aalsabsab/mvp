﻿using Autofac.Extras.Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tharwat.DataAccess;
using Tharwat.Views;
using Tharwat.Views.Models;
using Xunit;

namespace Tharwat.Presenters.Tests
{
    public class BaseRepoPesentersTests
    {
        public BaseRepoPesentersTests()
        {

        }

        //[Fact]
        //public async void MethodTests()
        //{
        //    using (var mock = AutoMock.GetLoose())
        //    {
        //        mock.Mock<IRepositoryAsync<TestModel>>()
        //            .Setup(x => x.GetAllAsync(false))
        //            .Returns(GetSampleData());

        //        var x = mock.Mock<IListView<BaseDto>>().Raise(x => x.Loaded += null, new EventArgs());

        //        mock.Mock


        //        var cls = mock.Create<RepoPresenter>();
        //        var expected = await GetSampleData();
        //        var result = await cls.
        //    }
        //}

        private Task<List<TestModel>> GetSampleData()
        {
            var output = new List<TestModel>
            {
                new TestModel
                {
                    Id = 1,
                    Name = "Test1",
                    Status = "1"
                },
                new TestModel
                {
                    Id =2,
                    Name = "Test2",
                    Status = "1"
                }
            };
            return Task.FromResult(output);
        }
    }
}


