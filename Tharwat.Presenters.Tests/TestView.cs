﻿using System;
using System.Collections.Generic;
using System.Windows;
using Tharwat.Views;

namespace Tharwat.Presenters.Tests
{
    public class TestView : IListView<TestDto>
    {
        public TestDto Item => throw new NotImplementedException();

        public event EventHandler<TestDto> Inserting;
        public event EventHandler<TestDto> Updating;
        public event EventHandler<TestDto> Deleting;
        public event EventHandler<TestDto> Expanding;
        public event EventHandler Refreshing;
        public event RoutedEventHandler Loaded;

        public void BindCollection(ICollection<TestDto> collection)
        {
            throw new NotImplementedException();
        }

        public void OperationEditResponse(bool isOk, string message)
        {
            throw new NotImplementedException();
        }

        public void ShowErrorMessage(string value)
        {
            throw new NotImplementedException();
        }
    }
}