﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tharwat.Presenters;
using Tharwat.DataAccess;
using Tharwat.Views;
using Tharwat.Views.Models;
using System.Linq.Expressions;

namespace Tharwat.Presenters
{
    ///<summary>
    /// This class is an automated way for adding dublicated code in all the presenters that uses IRepositoryAsync<TModel> Data Access interface
    /// 
    /// This class automate the presenter that connect the View and the DataAccess basic operations events (Insert,Update,Delete,Loading,Refreshing and expanding)
    /// This class addes BindList mothod for an easy way to bind list that are needed in the View
    /// </summary>
    /// <typeparam name="TView">The view interfaces of the presenter wich implements IListView interface</typeparam>
    /// <typeparam name="TDto">The DTO model class of the view</typeparam>
    /// <typeparam name="TModel">The Model class of the data access</typeparam>
    public class BaseRepoPresenter<TView, TDto, TModel> : IPresenter<TView>
        where TView : IListView<TDto>
        where TModel : class
    {
        protected readonly IRepositoryAsync<TModel> store;
        protected readonly IMapper mapper;

        public TView View { get; private set; }

        /// <summary>
        /// Set to true will load all the details without lazy loading
        /// Optimal for spacial cases like including inverse navigation properity
        /// </summary>
        public bool LoadAllDetails { get; protected set; }

        public BaseRepoPresenter(TView view, IRepositoryAsync<TModel> store, IMapper mapper)
        {
            View = view;
            this.store = store;
            this.mapper = mapper;
            Bind(view, store, mapper);
        }

        protected void Bind(TView view, IRepositoryAsync<TModel> store, IMapper mapper)
        {
            var mainList = new ObservableCollection<TDto>();

            view.Expanding += async (s, item) =>
            {
                var id = typeof(TDto).GetProperty("Id")?.GetValue(item);

                var dbItem = await store.GetByIdAsync(id);
                var index = mainList.IndexOf(item);
                item = mapper.Map<TDto>(dbItem);
                mainList[index] = item;
            };
            view.Loaded += async (s, e) =>
            {
                mainList = await LoadDataAsync();


                View.BindCollection(mainList);
            };

            view.Refreshing += async (s, e) =>
            {
                mainList = await LoadDataAsync();
                View.BindCollection(mainList);
            };

            view.Inserting += async (s, item) =>
            {

                var dbItem = mapper.Map<TModel>(item);

                try
                {
                    await store.InsertAsync(dbItem);
                    item = mapper.Map<TDto>(dbItem);
                    mainList.Add(item);
                    View.OperationEditResponse(true, "");
                }
                catch (Exception ex)
                {
                    ShowEditErrorMessage(ex);
                }
            };

            view.Updating += async (s, item) =>
            {
                var dbItem = mapper.Map<TModel>(item);

                var id = typeof(TDto).GetProperty("Id")?.GetValue(item);

                item = mainList.FirstOrDefault(ExpressionBuilder.BuildPredicate<TDto>("Id", "==", id).Compile());
                var index = mainList.IndexOf(item);
                try
                {
                    await store.UpdateAsync(dbItem);
                    item = mapper.Map<TDto>(dbItem);
                    mainList[index] = item;
                    view.OperationEditResponse(true, "");
                }
                catch (Exception ex)
                {
                    ShowEditErrorMessage(ex);
                }
            };

            view.Deleting += async (s, item) =>
            {
                var dbItem = mapper.Map<TModel>(item);

                try
                {
                    await store.DeleteAsync(dbItem);
                    mainList.Remove(item);
                }
                catch (Exception ex)
                {
                    view.ShowErrorMessage(ex.Message);
                }
            };
        }

        protected void ShowEditErrorMessage(Exception ex)
            => View.OperationEditResponse(false, $"{ex.Message} Details: {ex.InnerException?.Message}");
        protected void ShowEditErrorMessage(string message)
            => View.OperationEditResponse(false, message);
        private async Task<ObservableCollection<TDto>> LoadDataAsync()
        {
            List<TModel> dbModels = await GetDbDataAsync();
            var mainList = mapper.Map<ObservableCollection<TDto>>(dbModels);
            return mainList;
        }

        protected virtual async Task<List<TModel>> GetDbDataAsync() => await store.GetAllAsync(LoadAllDetails);

        /// <summary>
        /// An easy and automated way to bind the list to the View and its mappings
        /// </summary>
        /// <typeparam name="TDtoList">DTO of the list objects</typeparam>
        /// <typeparam name="TModelList">Model of the list objects</typeparam>
        /// <param name="bindListAction"></param>
        protected void BindList<TDtoList, TModelList>(Action<ICollection<TDtoList>> bindListAction, bool isEager = false)
            where TModelList : class

        {
            View.Loaded += async (s, e) =>
            {
                var listDb = await store.GetListAsync<TModelList>(isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
            View.Refreshing += async (s, e) =>
            {
                var listDb = await store.GetListAsync<TModelList>(isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
        }

        /// <summary>
        /// An easy and automated way to bind the list to the View and its mappings
        /// 
        /// This overload is created for list that are taken from another context
        /// </summary>
        /// <typeparam name="TDtoList">DTO of the list objects</typeparam>
        /// <typeparam name="TModelList">Model of the list objects</typeparam>
        /// <param name="customStore"></param>
        /// <param name="bindListAction"></param>
        protected void BindList<TDtoList, TModelList>(IRepositoryAsync<TModelList> customStore, Action<ICollection<TDtoList>> bindListAction, bool isEager = false)
            where TModelList : class

        {
            View.Loaded += async (s, e) =>
            {
                var listDb = await customStore.GetListAsync<TModelList>(isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
            View.Refreshing += async (s, e) =>
            {
                var listDb = await customStore.GetListAsync<TModelList>(isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
        }


        /// <summary>
        /// An easy and automated way to bind the list to the View and its mappings
        /// </summary>
        /// <typeparam name="TDtoList">DTO of the list objects</typeparam>
        /// <typeparam name="TModelList">Model of the list objects</typeparam>
        /// <param name="bindListAction"></param>
        /// <param name="customParameters"></param>
        protected void BindList<TDtoList, TModelList>(Action<ICollection<TDtoList>> bindListAction, string customParameters, bool isEager = false)
            where TModelList : class
        {
            View.Loaded += async (s, e) =>
            {
                var listDb = await store.GetListAsync<TModelList>(customParameters, isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
            View.Refreshing += async (s, e) =>
            {
                var listDb = await store.GetListAsync<TModelList>(customParameters, isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
        }

        /// <summary>
        /// An easy and automated way to bind the list to the View and its mappings
        /// </summary>
        /// <typeparam name="TDtoList">DTO of the list objects</typeparam>
        /// <typeparam name="TModelList">Model of the list objects</typeparam>
        /// <param name="bindListAction"></param>
        /// <param name="customParameters"></param>
        protected void BindList<TDtoList, TModelList>(Action<ICollection<TDtoList>> bindListAction, Expression<Func<TModelList, TModelList>> selector, bool isEager = false)
            where TModelList : class
        {
            View.Loaded += async (s, e) =>
            {
                var listDb = await store.GetListAsync(selector, isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
            View.Refreshing += async (s, e) =>
            {
                var listDb = await store.GetListAsync(selector, isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
        }

        protected void BindList<TDtoList, TModelList>(Action<ICollection<TDtoList>> bindListAction, Expression<Func<TModelList, bool>> predicate, Expression<Func<TModelList, TModelList>> selector, bool isEager = false)
           where TModelList : class
        {
            View.Loaded += async (s, e) =>
            {
                var listDb = await store.GetListAsync(predicate, selector, isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
            View.Refreshing += async (s, e) =>
            {
                var listDb = await store.GetListAsync(predicate, selector, isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
        }



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDtoList"></typeparam>
        /// <typeparam name="TModelList"></typeparam>
        /// <param name="customStore"></param>
        /// <param name="bindListAction"></param>
        /// <param name="customParameters"></param>
        protected void BindList<TDtoList, TModelList>(IRepositoryAsync<TModelList> customStore, Action<ICollection<TDtoList>> bindListAction, string customParameters, bool isEager = false)
            where TModelList : class
        {
            View.Loaded += async (s, e) =>
            {
                var listDb = await customStore.GetListAsync<TModelList>(customParameters, isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
            View.Refreshing += async (s, e) =>
            {
                var listDb = await customStore.GetListAsync<TModelList>(customParameters, isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
        }


    }
}