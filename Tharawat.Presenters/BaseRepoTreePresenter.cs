﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tharwat.DataAccess;
using Tharwat.Views;
using Tharwat.Views.Models;

namespace Tharwat.Presenters
{
    public class BaseRepoTreePresenter<TView, TDto, TModel> : BaseRepoPresenter<TView, TDto, TModel>
        where TView : IListView<TDto>
        where TModel : class
    {
        private readonly IRepositoryTreeAsync<TModel> treeStore;

        public BaseRepoTreePresenter(TView view, IRepositoryTreeAsync<TModel> store, IMapper mapper) : base(view, store, mapper)
        {
            treeStore = store;
        }
        protected void BindTreeList<TDtoList, TModelList>(bool isMain, Action<ICollection<TDtoList>> bindListAction, bool isEager = false) where TModelList : class
        {
            View.Loaded += async (s, e) =>
            {
                var listDb = await treeStore.GetListTreeAsync<TModelList>(isMain, isEager);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
            View.Refreshing += async (s, e) =>
            {
                var listDb = await treeStore.GetListTreeAsync<TModelList>(isMain);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
        }
        protected void BindTreeList<TDtoList, TModelList>(bool isMain, Action<ICollection<TDtoList>> bindListAction, string customParameters) where TModelList : class
        {
            View.Loaded += async (s, e) =>
            {
                var listDb = await treeStore.GetListTreeAsync<TModelList>(isMain, customParameters);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
            View.Refreshing += async (s, e) =>
            {
                var listDb = await treeStore.GetListTreeAsync<TModelList>(isMain, customParameters);
                var listDto = mapper.Map<List<TDtoList>>(listDb);
                bindListAction.Invoke(listDto);
            };
        }
    }
}
