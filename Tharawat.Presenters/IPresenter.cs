﻿using System.Collections;
using System.Collections.Generic;
using Tharwat.Views;

namespace Tharwat.Presenters
{
    public interface IPresenter<T> where T : IView
    {
        T View { get; }
    }

    public interface IPresenter<T, TDto,TModel> : IPresenter<T>
        where T : IListView< TDto> 
        where TModel : class 
    {

    }

    public interface IPresenter<T, TDto, TModel,TDtoList1,TModelList1> : IPresenter<T, TDto, TModel>
    where T : IListView<TDto>
    where TModel : class
    where TModelList1 : class
    {
    }
}
