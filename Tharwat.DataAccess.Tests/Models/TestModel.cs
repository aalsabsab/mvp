﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tharwat.DataAccess.Tests
{
    public class TestModel
    {
        [Key]
        public decimal Id { get; set; }
        public string Name { get; set; }
        public string OtherData { get; set; }
        public string StringId => Id.ToString();
        public string Status { get; set; }

        public ICollection<ChildModel> ChildModels { get; set; } 
    }
}