﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tharwat.DataAccess.Tests
{
    public class ChildModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TestModelId { get; set; }
        public TestModel TestModel { get; set; }
    }
}
