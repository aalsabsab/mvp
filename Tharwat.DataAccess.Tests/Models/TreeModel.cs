﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Tharwat.DataAccess.Tests
{
    public partial class TreeModel
    {
        public TreeModel()
        {
            InverseParent = new HashSet<TreeModel>();
        }

        public decimal Id { get; set; }
        public decimal? ParentId { get; set; }
        public string IsMain { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string Status { get; set; }
        public virtual ICollection<TreeModel> InverseParent { get; set; }
    }
}
