﻿using System;

namespace Tharwat.DataAccess.Tests
{
    public class NoStatusModel
    {
        public decimal Id { get; set; }
        public string Name { get; set; }
        public string OtherData { get; set; }
    }

}