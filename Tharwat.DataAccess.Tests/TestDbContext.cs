﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Tharwat.DataAccess.Tests
{
    public class TestDbContext : DbContext
    {
        public TestDbContext(DbContextOptions<TestDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TestModel> TestSet { get; set; }
        public virtual DbSet<NoStatusModel> NoStatusSet { get; set; }
        public virtual DbSet<ChildModel> ChildModelsSet { get; set; }
        public virtual DbSet<TreeModel> TreeModelsSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo((message)=> {
                var m = message;
            });
            base.OnConfiguring(optionsBuilder);
        }
    }
}