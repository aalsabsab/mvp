﻿using EntityFrameworkCoreMock;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Tharwat.DataAccess.Tests
{
    public class BaseSystemContextStoreTests
    {
        public BaseSystemContextStoreTests()
        {
        }
        public DbContextOptions<TestDbContext> DummyOptions { get; } = new DbContextOptionsBuilder<TestDbContext>().Options;

        [Fact]
        public async void GetAllTest()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);

            var actual = await store.GetAllAsync<TestModel>(eager: false);
            Assert.NotNull(actual);
            Assert.Equal(expected.Count(), actual.Count());
            Assert.Equal(actual[0].Name, expected[0].Name);
        }
        [Fact]
        public async void GetAllTestCustom()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);

            var actual = await store.GetAllAsync<TestModel>(c => c.Id == 1);
            Assert.NotNull(actual);
            Assert.Single(actual);
            Assert.Equal(actual[0].Name, expected[0].Name);
            Assert.Throws<ArgumentOutOfRangeException>(() => actual[1].Name);
        }
        [Fact]
        public async void GetByIdTest()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);

            var actual = await store.GetByIdAsync<TestModel, decimal>(1, false);
            Assert.Equal(1, actual.Id);
        }
        [Fact]
        public async void InsertTest()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);

            await store.InsertAsync(new TestModel { Id = 3, Name = "Whatever", Status = "1" });

            var actual = await store.GetListAsync<TestModel>();
            Assert.Equal(3, actual.Count);
            Assert.Equal("Whatever", actual[2].Name);
        }
        [Fact]
        public async void UpdateTest()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);

            var updated = expected[0];
            updated.Name = "Edited name";
            await store.UpdateAsync(updated);

            var actual = await store.GetListAsync<TestModel>();
            Assert.Equal(2, actual.Count);
            Assert.Equal(updated.Name, actual[0].Name);
        }

        [Fact]
        public async void DeleteTest()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);

            await store.DeleteAsync(expected[0]);

            var actual = await store.GetListAsync<TestModel>();
            Assert.Equal(expected.Count() - 1, actual.Count);

            Assert.Equal(expected[1].Name, actual[0].Name);
        }

        [Fact]
        public async void GetListTest()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);

            var actual = await store.GetListAsync<TestModel>();

            Assert.NotNull(actual);
            Assert.Equal(actual.Count(), expected.Count());
            Assert.Null(actual[0].OtherData);
            Assert.Equal(actual[0].Name, expected[0].Name);
        }

        [Fact]
        public async void GetListTestNoStatus()
        {
            var expected = new[]
            {
                new NoStatusModel{ Id = 1, Name = "Obj1", OtherData = "otherData1" },
                new NoStatusModel{ Id = 2, Name = "Obj2", OtherData = "otherData2" },
            };
            var dbContextMock = new DbContextMock<TestDbContext>(DummyOptions);
            var testDbSetMock = dbContextMock.CreateDbSetMock(x => x.NoStatusSet, expected);

            var store = new SystemContextStore(() => dbContextMock.Object);

            var actual = await store.GetListAsync<NoStatusModel>();

            Assert.NotNull(actual);
            Assert.Equal(expected.Count(), actual.Count());
            Assert.Null(actual[0].OtherData);
            Assert.Equal(expected[0].Name, actual[0].Name);
        }

        [Fact]
        public async void IsExistsTests()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);

            var isExist = await store.IsExists<TestModel>(t => t.Id == expected[0].Id);

            Assert.True(isExist);
        }

        [Fact]
        public async void GetTreeListNotMain()
        {
            InitContext(out TreeModel[] expected, out SystemContextStore store);

            var actual = await store.GetListTreeAsync<TreeModel>(false);
            Assert.NotNull(actual);
            int expectedCount = expected.Where(t => t.IsMain == "0").Count();
            Assert.Equal(expectedCount, actual.Count());
            Assert.Equal(1, actual[0].ParentId);
        }
        private void InitContext(out TreeModel[] initialEntities, out SystemContextStore store)
        {
            initialEntities = new[]
            {
                new TreeModel{ Id = 1, Name = "Obj1", Note = "otherData1", IsMain = "1", Status = "1" },
                new TreeModel{ Id = 2, Name = "Obj2", Note = "otherData1", IsMain = "0", ParentId = 1, Status = "1" },
                new TreeModel{ Id = 3, Name = "Obj3", Note = "otherData1", IsMain = "0", ParentId = 1, Status = "1" },
                new TreeModel{ Id = 4, Name = "Obj4", Note = "otherData1", IsMain = "0", ParentId = 1, Status = "1" },
            };

            var dbContextMock = new DbContextMock<TestDbContext>(DummyOptions);
            var testDbSetMock = dbContextMock.CreateDbSetMock(x => x.TreeModelsSet, initialEntities);
            store = new SystemContextStore(() => dbContextMock.Object);

            var context = dbContextMock.Object;
            var dbset = context.Set<TreeModel>();
            var model = context.Model;
        }

        private void InitContext(out TestModel[] initialEntities, out SystemContextStore store, int count)
        {
            initialEntities = new TestModel[count];
            for (int i = 0; i < count; i++)
            {
                initialEntities[i] = new TestModel { Id = i + 1, Name = $"Obj{i + 1}", OtherData = $"OtherData{i + 1}" };
            }

            if (count > 0)
            {
                var childs = new List<ChildModel>
                {
                    new ChildModel { Id = 1, Name = "First" },
                    new ChildModel { Id = 2, Name = "Second" },
                    new ChildModel { Id = 3, Name = "Third" },
                };
                initialEntities[0].ChildModels = childs;
            }


            var dbContextMock = new DbContextMock<TestDbContext>(DummyOptions);
            var testDbSetMock = dbContextMock.CreateDbSetMock(x => x.TestSet, initialEntities);
            //var testDbSetMock2 = dbContextMock.CreateDbSetMock(x => x.ChildModelsSet, childs);

            store = new SystemContextStore(() => dbContextMock.Object);

            var context = dbContextMock.Object;
            var dbset = context.Set<TestModel>();
            var model = context.Model;
        }

        [Fact]
        public async void GetListCustomTest()
        {
            InitContext(out TestModel[] expected, out SystemContextStore cls, 2);

            var actual = await cls.GetListAsync<TestModel>("Id,OtherData");

            Assert.NotNull(actual);
            Assert.Equal(actual.Count(), expected.Count());
            Assert.Null(actual[0].Name);
            Assert.Equal(actual[0].OtherData, expected[0].OtherData);
        }
        [Fact]
        public async void MaxTest()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);

            var actual = await store.MaxAsync<TestModel, decimal>(t => t.Id);

            Assert.Equal(2, actual);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(9)]
        [InlineData(10)]
        [InlineData(100)]
        public async void MaxStringTest(int count)
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, count);

            var actual = await store.MaxStringAsync<TestModel>(t => t.StringId);

            Assert.Equal(count.ToString(), actual);
        }
        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(9)]
        [InlineData(10)]
        [InlineData(100)]
        [InlineData(1000)]
        public async void MaxIntTest(int count)
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, count);

            var actual = await store.MaxAsync<TestModel, decimal>(t => t.Id);

            Assert.Equal(count, actual);
        }
        [Fact]
        public async void MaxTestNull()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 0);

            await Assert.ThrowsAsync<InvalidOperationException>(async () => await store.MaxAsync<TestModel, decimal>(t => t.Id));
        }
        [Fact]
        public async Task ShouldRolebackBeginTransaction()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);
            try
            {
                var tasks = new Task[] {
                    store.InsertAsync(new TestModel { Id = 3 }),
                    store.InsertAsync(new TestModel { Id = 4 }),
                    store.InsertAsync(new TestModel { Id = 5 }),
                    store.InsertAsync(new TestModel { Id = 4 }),
                };
            }
            catch (Exception)
            {
                //throw;
            }
            var data = await store.GetListAsync<TestModel>();

            Assert.Equal(2, data.Count);
        }
        [Fact]
        public async Task ShouldRolebackInsert()
        {
            InitContext(out TestModel[] expected, out SystemContextStore store, 2);
            try
            {
                await store.BeginTransaction();
                await store.InsertAsync(new TestModel { Id = 3 });
                await store.InsertAsync(new TestModel { Id = 4 });
                await store.InsertAsync(new TestModel { Id = 5 });
                await store.InsertAsync(new TestModel { Id = 4 });
                await store.Commit();
            }
            catch (Exception)
            {
                throw;
            }
            var data = await store.GetListAsync<TestModel>();

            Assert.Equal(2, data.Count);
        }
    }
}
