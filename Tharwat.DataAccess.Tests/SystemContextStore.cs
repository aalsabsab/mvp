﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tharwat.DataAccess.Tests
{
    public class SystemContextStore : BaseSystemContextStore
    {
        private readonly Func<TestDbContext> contextFunc;

        public SystemContextStore(Func<TestDbContext> contextFunc) : base(contextFunc)
        {
            this.contextFunc = contextFunc;
        }
    }
}
