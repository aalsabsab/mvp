﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tharwat.DataAccess
{
    public interface IRepositoryBasicAsync<T> where T : class
    {
        Task<List<T>> GetAllAsync(bool eager = false);
        Task<int> InsertAsync(T value);
        Task<int> UpdateAsync(T value);
        Task<int> DeleteAsync(T value);
        Task<T> GetByIdAsync<TypeId>(TypeId id, bool isEager = true);
    }
}
