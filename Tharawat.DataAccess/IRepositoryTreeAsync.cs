﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tharwat.DataAccess
{
    public interface IRepositoryTreeAsync<T> : IRepositoryAsync<T> where T : class
    {
        Task<List<TListModel>> GetListTreeAsync<TListModel>(bool isMain, bool isEager = false) where TListModel : class;
        Task<List<TListModel>> GetListTreeAsync<TListModel>(bool isMain, string customParameters, bool isEager = false) where TListModel : class;
    }
}
