﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Tharwat.DataAccess
{
    public interface ISystemRepoAsync
    {
        Task<int> DeleteAsync<T>(T value) where T : class;
        Task<List<T>> GetAllAsync<T>(bool eager = false) where T : class;
        Task<List<T>> GetAllAsync<T>(Expression<Func<T, bool>> predicate, bool eager = false) where T : class;
        Task<T> GetByIdAsync<T>(decimal id) where T : class;
        Task<List<T>> GetListAsync<T>(bool isEager = false) where T : class;
        Task<List<T>> GetListAsync<T>(string customParameters, bool isEager = false) where T : class;
        Task<List<T>> GetListTreeAsync<T>(bool isMain, bool isEager = false) where T : class;
        Task<List<T>> GetListTreeAsync<T>(bool isMain, string customParameters, bool isEager = false) where T : class;
        Task<int> InsertAsync<T>(T value) where T : class;
        Task<int> UpdateAsync<T>(T value) where T : class;
        Task<int> ExecuteSqlRawAsync(string sql, params object[] parameters);
        Task<bool> IsExists<T>(Expression<Func<T, bool>> predicate) where T : class;
        Task<List<T>> GetListAsync<T>(Expression<Func<T, T>> selector, bool isEager = false) where T : class;
        Task<List<T>> GetListAsync<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, T>> selector, bool isEager = false) where T : class;
        Task<T> GetByIdAsync<T>(int id) where T : class;
        Task<TResult> MaxAsync<T, TResult>(Expression<Func<T, TResult>> predicate) where T : class;
        Task<T> GetByIdAsync<T, TypeId>(TypeId id, bool isEager = true) where T : class;
        Task<List<TResult>> GetListAsync<T, TResult>(Expression<Func<T, TResult>> selector, bool isEager = false) where T : class;
        Task<List<TResult>> GetListAsync<T, TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector, bool isEager = false) where T : class;
        Task<List<T>> GetListAsync<T>(Expression<Func<T, bool>> predicate, bool isEager = false) where T : class;
        IQueryable<T> Query<T>() where T : class;
        Task BeginTransaction();
        Task Commit();
        Task RoleBack();
        Task<int> CountAsync<T>(Expression<Func<T, bool>> predicate) where T : class;
        Task<long> CountLongAsync<T>(Expression<Func<T, bool>> predicate) where T : class;
        Task<List<T>> RunQueryAsync<T>(IQueryable<T> query);
        Task<int> ExecuteSqlInterpolatedAsync(FormattableString sql);
        Task<List<T>> FromSqlRaw<T>(string sql) where T : class;
        Task<List<T>> FromSqlInterpolated<T>(FormattableString sql) where T : class;
        Task<TResult> MaxAsync<T, TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector) where T : class;
        Task<T> RunQueryFirstOrDefaultAsync<T>(IQueryable<T> query);
    }
}