﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Tharwat.DataAccess
{
    public abstract class BaseRepositoryStore<T> : IRepositoryAsync<T> where T : class
    {
        protected readonly ISystemRepoAsync contextStore;

        public BaseRepositoryStore(ISystemRepoAsync store)
        {
            this.contextStore = store;
        }

        public IQueryable<T> Query() => contextStore.Query<T>();

        public IQueryable<TModel> Query<TModel>() where TModel : class => contextStore.Query<TModel>();

        public virtual Task<List<T>> GetAllAsync(bool eager = false)
            => contextStore.GetAllAsync<T>(eager);
        public virtual Task<List<T>> GetAllAsync(Expression<Func<T, bool>> where, bool eager = false)
            => contextStore.GetAllAsync(where, eager);

        public virtual Task<T> GetByIdAsync<TypeId>(TypeId id, bool isEager = true)
            => contextStore.GetByIdAsync<T, TypeId>(id, isEager);

        public virtual Task<List<TListModel>> GetListAsync<TListModel>(bool isEager = false) where TListModel : class
            => contextStore.GetListAsync<TListModel>(isEager);
        public Task<List<TListModel>> GetListAsync<TListModel>(string customParameters, bool isEager = false) where TListModel : class
            => contextStore.GetListAsync<TListModel>(customParameters, isEager);

        public Task<List<TListModel>> GetListAsync<TListModel>(Expression<Func<TListModel, TListModel>> selector, bool isEager = false) where TListModel : class
            => contextStore.GetListAsync<TListModel>(selector, isEager);
        public Task<List<TListModel>> GetListAsync<TListModel>(Expression<Func<TListModel, bool>> predicate, Expression<Func<TListModel, TListModel>> selector, bool isEager = false) where TListModel : class
            => contextStore.GetListAsync<TListModel>(predicate, selector, isEager);
        public Task<List<TListModel>> GetListAsync<TListModel>(Expression<Func<TListModel, bool>> predicate, bool isEager = false) where TListModel : class
            => contextStore.GetListAsync(predicate, isEager);
        public Task<List<TResult>> GetListAsync<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector, bool isEager = false)
            => contextStore.GetListAsync(predicate, selector, isEager);
        public Task<List<TResult>> GetListAsync<TResult>(Expression<Func<T, TResult>> selector, bool isEager = false)
            => contextStore.GetListAsync(selector, isEager);

        public Task<List<TModel>> RunQueryAsync<TModel>(IQueryable<TModel> query) => contextStore.RunQueryAsync(query);

        public Task<TModel> RunQueryFirstOrDefaultAsync<TModel>(IQueryable<TModel> query) => contextStore.RunQueryFirstOrDefaultAsync(query);

        public virtual async Task<int> InsertAsync(T value)
        {
            await PreInserting(value);
            var result = await contextStore.InsertAsync(value);
            await PostInserting(value);
            return result;
        }

        public virtual async Task<int> UpdateAsync(T value)
        {
            await PreUpdating(value);
            var result = await contextStore.UpdateAsync(value);
            await PostUpdating(value);
            return result;
        }
        public virtual async Task<int> DeleteAsync(T value)
        {
            await PreDeleting(value);
            var result = await contextStore.DeleteAsync(value);
            await PostUpdating(value);
            return result;
        }
        protected virtual Task PreInserting(T value) { return Task.CompletedTask; }
        protected virtual Task PreUpdating(T value) { return Task.CompletedTask; }
        protected virtual Task PreDeleting(T value) { return Task.CompletedTask; }

        protected virtual Task PostInserting(T value) { return Task.CompletedTask; }
        protected virtual Task PostUpdating(T value) { return Task.CompletedTask; }
        protected virtual Task PostDeleting(T value) { return Task.CompletedTask; }
    }
}