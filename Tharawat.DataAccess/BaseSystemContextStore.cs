﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Tharwat.DataAccess
{
    public abstract class BaseSystemContextStore : ISystemRepoAsync
    {
        private readonly Func<DbContext> contextFunc;
        private DbContext contextTransaction;

        //private DbContext contextTransaction;
        private IDbContextTransaction transaction;
        private DbContext contextQuery;

        public BaseSystemContextStore(Func<DbContext> contextFunc)
        {
            this.contextFunc = contextFunc;
        }
        private static Expression<Func<T, T>> GetListSelector<T>() where T : class
            => ExpressionBuilder.BuildSelector<T>("Id,Name");

        private static Expression<Func<T, bool>> GetHasStatusPredicate<T>() where T : class
            => ExpressionBuilder.BuildPredicate<T>(HasStringStatus<T>() ? "Status" : null, "!=", "0");

        private static Expression<Func<T, T>> GetCustomSelector<T>(string customParameters) where T : class
        {
            return ExpressionBuilder.BuildSelector<T>(customParameters);
        }

        private static Expression<Func<T, bool>> GetIsMainPredicate<T>(bool isMain) where T : class
        {
            return ExpressionBuilder.BuildPredicate<T>("IsMain", isMain ? "==" : "!=", "1");
        }
        private static bool HasStringStatus<T>() where T : class
        {
            var type = typeof(T);
            var prop = type.GetProperty("Status");
            return prop != null && prop?.PropertyType == typeof(string);
        }

        public async Task<List<T>> GetListAsync<T>(bool isEager = false) where T : class
        {
            using var context = contextFunc();

            var result = await context.Query<T>(GetHasStatusPredicate<T>(), GetListSelector<T>(), isEager).ToListAsyncSafe();
            return result;
        }


        public async Task<List<T>> GetListAsync<T>(string customParameters, bool isEager = false) where T : class
        {
            using var context = contextFunc();

            var result = await context.Query<T>(GetHasStatusPredicate<T>(), GetCustomSelector<T>(customParameters), isEager).ToListAsyncSafe();

            return result;
        }

        public async Task<List<T>> GetListAsync<T>(Expression<Func<T, T>> selector, bool isEager = false) where T : class
        {
            using var context = contextFunc();

            var result = await context.Query<T>(selector, isEager).ToListAsyncSafe();

            return result;
        }

        public async Task<List<T>> GetListAsync<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, T>> selector, bool isEager = false) where T : class
        {
            using var context = contextFunc();

            var result = await context.Query<T>(predicate, selector, isEager).ToListAsyncSafe();

            return result;
        }

        public async Task<List<TResult>> GetListAsync<T, TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector, bool isEager = false) where T : class
        {
            using var context = contextFunc();

            var result = await context.Query(predicate, selector, isEager).ToListAsyncSafe();

            return result;
        }

        public async Task<List<TResult>> GetListAsync<T, TResult>(Expression<Func<T, TResult>> selector, bool isEager = false) where T : class
        {
            using var context = contextFunc();

            var result = await context.Query(selector, isEager).ToListAsyncSafe();

            return result;
        }

        public async Task<List<T>> GetListAsync<T>(Expression<Func<T, bool>> predicate, bool isEager = false) where T : class
        {
            using var context = contextFunc();

            var result = await context.Query<T>(predicate, GetListSelector<T>(), isEager).ToListAsyncSafe();

            return result;
        }

        public async Task<List<T>> GetListTreeAsync<T>(bool isMain, bool isEager = false) where T : class
        {
            using var context = contextFunc();
            var result = await context.Query<T>(GetIsMainPredicate<T>(isMain), GetListSelector<T>(), isEager).ToListAsyncSafe();
            return result;
        }

        public async Task<List<T>> GetListTreeAsync<T>(bool isMain, string customParameters, bool isEager = false) where T : class
        {
            using var context = contextFunc();
            var result = await context.Query<T>(GetIsMainPredicate<T>(isMain), GetCustomSelector<T>(customParameters), isEager).ToListAsyncSafe();
            return result;
        }

        public async Task<List<T>> GetAllAsync<T>(bool eager = false) where T : class
        {
            using var context = contextFunc();
            var result = await context.Query<T>(eager).ToListAsyncSafe();
            return result;
        }


        public async Task<List<T>> GetAllAsync<T>(Expression<Func<T, bool>> predicate, bool eager = false) where T : class
        {
            using var context = contextFunc();
            var result = await context.Query(predicate, eager).ToListAsyncSafe();
            return result;
        }

        public IQueryable<T> Query<T>() where T : class
        {
            if (contextQuery == null)
                contextQuery = contextFunc();

            var query = contextQuery.Set<T>().AsQueryable();

            return query;
        }

        public async Task<List<T>> RunQueryAsync<T>(IQueryable<T> query)
        {
            var data = await query.ToListAsyncSafe();
            await contextQuery.DisposeAsync();
            contextQuery = null;
            return data;
        } 
        
        public async Task<T> RunQueryFirstOrDefaultAsync<T>(IQueryable<T> query)
        {
            var data = await query.FirstOrDefaultAsyncSafe();
            await contextQuery.DisposeAsync();
            contextQuery = null;
            return data;
        }

        public async Task<T> GetByIdAsync<T>(int id) where T : class
        {
            using var context = contextFunc();

            var result = await context.Query(ExpressionBuilder.BuildPredicate<T>("Id", "==", id), true).AsNoTracking()
                .FirstOrDefaultAsyncSafe();
            return result;
        }
        public async Task<T> GetByIdAsync<T>(decimal id) where T : class
        {
            using var context = contextFunc();
            var result = await context.Query(ExpressionBuilder.BuildPredicate<T>("Id", "==", id), true).AsNoTracking()
                .FirstOrDefaultAsyncSafe();
            return result;
        }

        public async Task<T> GetByIdAsync<T, TypeId>(TypeId id, bool isEager = true) where T : class
        {
            using var context = contextFunc();
            //var result = await context.FindAsync<T>(id);
            var result = await context.Query(ExpressionBuilder.BuildPredicate<T>("Id", "==", id), isEager).AsNoTracking()
                .FirstOrDefaultAsyncSafe();
            return result;
        }

        public async Task BeginTransaction()
        {
            contextTransaction = contextFunc();
            transaction = await contextTransaction.Database.BeginTransactionAsync();
        }

        public async Task Commit()
        {
            await transaction.CommitAsync();
            await transaction.DisposeAsync();
            await contextTransaction.DisposeAsync();
            contextTransaction = null;
        }

        public async Task RoleBack()
        {
            if (contextTransaction != null)
            {
                await transaction.RollbackAsync();
                transaction?.Dispose();
                contextTransaction?.Dispose();
                contextTransaction = null;
            }
            await Task.CompletedTask;
        }

        private void BindTransaction(DbContext context)
        {
            if (contextTransaction != null)
            {
                context.Database.SetDbConnection(contextTransaction.Database.GetDbConnection());
                context.Database.UseTransaction(transaction.GetDbTransaction());
            }
        }

        public async Task<int> InsertAsync<T>(T value) where T : class
        {
            using var context = contextFunc();

            BindTransaction(context);

            try
            {
                context.Set<T>().Add(value);
                return await context.SaveChangesAsync();
            }
            catch (Exception)
            {
                await RoleBack();
                throw;
            }
        }



        public async Task<int> UpdateAsync<T>(T value) where T : class
        {
            using var context = contextFunc();

            BindTransaction(context);

            try
            {
                var id = typeof(T).GetProperty("Id")?.GetValue(value);

                if (id is Guid guid)
                {

                    var oldValues = context.Set<T>().Find(id);


                    context.Entry(oldValues).CurrentValues.SetValues(value);
                }
                else
                    context.Set<T>().Update(value);

                return await context.SaveChangesAsync();
            }
            catch (Exception)
            {
                await RoleBack();
                throw;
            }
        }

        public async Task<int> DeleteAsync<T>(T value) where T : class
        {
            using var context = contextFunc();

            BindTransaction(context);

            try
            {
                context.Set<T>().Remove(value);
                return await context.SaveChangesAsync();
            }
            catch (Exception)
            {
                await RoleBack();
                throw;
            }
        }

        public async Task<bool> IsExists<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            using var context = contextFunc();

            var isExists = await context.Set<T>().AnyAsync(predicate);

            return isExists;
        }

        public async Task<TResult> MaxAsync<T, TResult>(Expression<Func<T, TResult>> selector) where T : class
        {
            using var context = contextFunc();

            var result = await context.Set<T>().MaxAsync(selector);

            return result;
        }
        
        public async Task<TResult> MaxAsync<T, TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector) where T : class
        {
            using var context = contextFunc();

            var result = await context.Set<T>().Where(predicate).MaxAsync(selector);

            return result;
        }

        public async Task<int> CountAsync<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            using var context = contextFunc();

            var result = await context.Set<T>().Where(predicate).CountAsync();

            return result;
        }

        public async Task<long> CountLongAsync<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            using var context = contextFunc();

            var result = await context.Set<T>().Where(predicate).LongCountAsync();

            return result;
        }

        public async Task<string> MaxStringAsync<T>(Expression<Func<T, string>> predicate) where T : class
        {
            using var context = contextFunc();

            IOrderedQueryable<T> ordered = context.Set<T>().OrderByDescending(predicate);
            var result = await ordered.Select(predicate).FirstOrDefaultAsyncSafe();
            var temp = Convert.ToInt32(result);
            //if (temp >= 9)
            //{
            //    temp +=1;
            //    result = temp.ToString();
            //}

            return result;
        }

        public async Task<int> ExecuteSqlRawAsync(string sql, params object[] parameters)
        {
            using var context = contextFunc();
            var resault = await context.Database.ExecuteSqlRawAsync(sql, parameters);
            return resault;
        }

        public async Task<int> ExecuteSqlInterpolatedAsync(FormattableString sql)
        {
            using var context = contextFunc();
            var resault = await context.Database.ExecuteSqlInterpolatedAsync(sql);
            return resault;
        } 
        
        public async Task<List<T>> FromSqlRaw<T>(string sql) where T : class
        {
            using var context = contextFunc();
            var resault =   context.Set<T>().FromSqlRaw(sql);
            return await resault.ToListAsyncSafe();
        }  
        
        public async Task<List<T>> FromSqlInterpolated<T>(FormattableString sql) where T : class
        {
            using var context = contextFunc();
            var resault =   context.Set<T>().FromSqlInterpolated(sql);
            return await resault.ToListAsyncSafe();
        }

        public void BeginTransaction(params Action[] actions)
        {
            using var context = contextFunc();
            using var transaction = context.Database.BeginTransaction();
            foreach (var action in actions)
            {
                try
                {
                    action.Invoke();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            transaction.Commit();
        }
        public async Task BeginTransaction(params Task[] actions)
        {
            using var context = contextFunc();
            using var transaction = await context.Database.BeginTransactionAsync();
            foreach (var action in actions)
            {
                try
                {
                    await action;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }
            }
            transaction.Commit();
        }

        public async Task BeginTransactionScope(params Task[] actions)
        {
            using var scope = new TransactionScope();

            foreach (var action in actions)
            {
                try
                {
                    await action;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            scope.Complete();
        }

    }
}
