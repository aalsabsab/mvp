﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Tharwat.DataAccess
{
    public static class QueryExtensions
    {
        public static IQueryable<T> Query<T>(this DbContext context, bool isEager = false) where T : class
        {
            var query = context.Set<T>().AsQueryable();
            query = context.ProcessEager(query, isEager);
            return query;
        }

        public static IQueryable<T> QueryInclude<T, TProperty>(this IQueryable<T> query, Expression<Func<T, TProperty>> navigationPropertyPath) where T : class
        {
            return query.Include(navigationPropertyPath);
        }

        public static IQueryable<T> Query<T>(this DbContext context, Expression<Func<T, T>> selector, bool isEager = false) where T : class
        {
            var query = context.Set<T>().Select(selector);
            query = context.ProcessEager(query, isEager);
            return query;
        }

        public static IQueryable<T> Query<T>(this DbContext context, Expression<Func<T, bool>> predicate, bool isEager = false) where T : class
        {
            var query = context.Set<T>().Where(predicate);
            query = context.ProcessEager(query, isEager);
            return query;
        }

        public static IQueryable<T> Query<T>(this DbContext context, Expression<Func<T, bool>> predicate, Expression<Func<T, T>> selector, bool isEager = false) where T : class
        {
            var query = context.Set<T>().Where(predicate);
            query = context.ProcessEager(query, isEager);
            return query.Select(selector);
        }

        public static IQueryable<TResult> Query<T, TResult>(this DbContext context, Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector, bool isEager = false) where T : class
        {
            var query = context.Set<T>().Where(predicate);
            query = context.ProcessEager(query, isEager);
            return query.Select(selector);
        }

        public static IQueryable<TResult> Query<T, TResult>(this DbContext context, Expression<Func<T, TResult>> selector, bool isEager = false) where T : class
        {
            var query = context.Set<T>().AsQueryable();
            query = context.ProcessEager(query, isEager);
            return query.Select(selector);
        }

        private static IQueryable<T> ProcessEager<T>(this DbContext context, IQueryable<T> query, bool isEager) where T : class
        {
            if (isEager)
            {
                var navigations = context.Model.FindEntityType(typeof(T))
                    .GetDerivedTypesInclusive()
                    .SelectMany(type => type.GetNavigations())

                    .Distinct();

                foreach (var property in navigations)
                    query = query.Include(property.Name).AsSingleQuery();
            }

            return query;
        }

        public static Task<List<TSource>> ToListAsyncSafe<TSource>(this IQueryable<TSource> source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (source as IAsyncEnumerable<TSource> == null)
                return Task.FromResult(source.ToList());
            return source.ToListAsync();
        }

        public static Task<TSource> FirstOrDefaultAsyncSafe<TSource>(this IQueryable<TSource> source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (source as IAsyncEnumerable<TSource> == null)
                return Task.FromResult(source.FirstOrDefault());
            return source.FirstOrDefaultAsync();
        }
    }
}
