﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Tharwat.DataAccess
{
    public interface IRepositoryAsync<T> : IRepositoryBasicAsync<T> where T : class
    {
        Task<List<TListModel>> GetListAsync<TListModel>(bool isEager = false) where TListModel : class;
        Task<List<TListModel>> GetListAsync<TListModel>(string customParameters, bool isEager = false) where TListModel : class;
        Task<List<T>> GetAllAsync(Expression<Func<T, bool>> where, bool eager = false);
        Task<List<TListModel>> GetListAsync<TListModel>(Expression<Func<TListModel, TListModel>> selector, bool isEager = false) where TListModel : class;
        Task<List<TListModel>> GetListAsync<TListModel>(Expression<Func<TListModel, bool>> predicate, Expression<Func<TListModel, TListModel>> selector, bool isEager = false) where TListModel : class;
        Task<List<TResult>> GetListAsync<TResult>(Expression<Func<T, bool>> predicate, Expression<Func<T, TResult>> selector, bool isEager = false);
        Task<List<TResult>> GetListAsync<TResult>(Expression<Func<T, TResult>> selector, bool isEager = false);
        Task<List<TListModel>> GetListAsync<TListModel>(Expression<Func<TListModel, bool>> predicate, bool isEager = false) where TListModel : class;
        Task<List<TModel>> RunQueryAsync<TModel>(IQueryable<TModel> query);
        Task<TModel> RunQueryFirstOrDefaultAsync<TModel>(IQueryable<TModel> query);
    }
}