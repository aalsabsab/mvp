﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tharwat.DataAccess
{
    public class BaseRepositoryTreeStore<T> : BaseRepositoryStore<T>, IRepositoryTreeAsync<T> where T : class
    {
        public BaseRepositoryTreeStore(ISystemRepoAsync store) : base(store)
        {
        }
        public Task<List<TListModel>> GetListTreeAsync<TListModel>(bool isMain, bool isEager = false) where TListModel : class
            => contextStore.GetListTreeAsync<TListModel>(isMain);

        public Task<List<TListModel>> GetListTreeAsync<TListModel>(bool isMain, string customParameters, bool isEager = false) where TListModel : class
            => contextStore.GetListTreeAsync<TListModel>(isMain, customParameters, isEager);
    }
}
