﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tharwat.DataAccess
{
    public interface IGetListAsync<T1>
    {
        Task<List<T1>> GetListAsync();
    }

    public interface IGetAutoAsync<T1> where T1 : class
    {
        Task GetListAsync(out List<T1> list);
    }

    public interface IGetListAsync<T1, T2> where T2 : IGetListAsync<T1> where T1 : class
    {
        Task GetListAsync(out List<T2> list);
    }
}
