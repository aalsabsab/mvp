﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tharwat.Views
{
    public interface IEditView<T> : IPartialView<T>
    {
        void InitItem(T item, bool isNewItem);

        event EventHandler<string> Success;
    }
    public interface IEditView<DtoItem, DtoList1> : IEditView<DtoItem>
    {
        void BindList(ICollection<DtoList1> list);
    }

    public interface IEditView<DtoItem, DtoList1, DtoList2> : IEditView<DtoItem, DtoList1>
    {
        void BindList(ICollection<DtoList2> list);
    }
    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3> : IEditView<DtoItem, DtoList1, DtoList2>
    {
        void BindList(ICollection<DtoList3> list);
    }
    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3>
    {
        void BindList(ICollection<DtoList4> list);
    }
    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4>
    {
        void BindList(ICollection<DtoList5> list);
    }
    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4,DtoList5>
    {
        void BindList(ICollection<DtoList6> list);
    }   
    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6>
    {
        void BindList(ICollection<DtoList7> list);
    }
    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7>
    {
        void BindList(ICollection<DtoList8> list);
    }
    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8>
    {
        void BindList(ICollection<DtoList9> list);
    }
    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9>
    {
        void BindList(ICollection<DtoList10> list);
    }

    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10>
    {
        void BindList(ICollection<DtoList11> list);
    }

    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11>
    {
        void BindList(ICollection<DtoList12> list);
    }

    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12>
    {
        void BindList(ICollection<DtoList13> list);
    }

    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13, DtoList14> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13>
    {
        void BindList(ICollection<DtoList14> list);
    }

    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13, DtoList14, DtoList15> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13, DtoList14>
    {
        void BindList(ICollection<DtoList15> list);
    }

    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13, DtoList14, DtoList15, DtoList16> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13, DtoList14, DtoList15>
    {
        void BindList(ICollection<DtoList16> list);
    }

    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13, DtoList14, DtoList15, DtoList16, DtoList17> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13, DtoList14, DtoList15, DtoList16>
    {
        void BindList(ICollection<DtoList17> list);
    }

    public interface IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13, DtoList14, DtoList15, DtoList16, DtoList17, DtoList18> : IEditView<DtoItem, DtoList1, DtoList2, DtoList3, DtoList4, DtoList5, DtoList6, DtoList7, DtoList8, DtoList9, DtoList10, DtoList11, DtoList12, DtoList13, DtoList14, DtoList15, DtoList16, DtoList17>
    {
        void BindList(ICollection<DtoList18> list);
    }
}
