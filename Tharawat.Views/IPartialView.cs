﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tharwat.Views
{
    public interface IPartialView<TDtoItem> : IView
    {
        TDtoItem Item { get; }
        event EventHandler<TDtoItem> Inserting;
        event EventHandler<TDtoItem> Updating;
        void OperationEditResponse(bool isOk, string message);
    }
}
