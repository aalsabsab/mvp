﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace Tharwat.Views
{
    public interface IView
    {
       event RoutedEventHandler Loaded;
    }
}
