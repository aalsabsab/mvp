﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tharwat.Views.Models
{
    public class BaseDto<TKey>
    {
        public TKey Id { get; set; }

        [Required, MinLength(4, ErrorMessage = "يجب إدخال هذا الحقل وأن لا يكون أقل من 4 أحرف!"), MaxLength(50, ErrorMessage = "لقد تجاوزت الحد المسموح")]
        public virtual string Name { get; set; }

        [MinLength(4, ErrorMessage = "يجب أن لا يكون الحقل أقل من 4 أحرف!")]
        [MaxLength(50, ErrorMessage = "لقد تجاوزت الحد المسموح")]
        public virtual string NameSecondary { get; set; }

        public string Status { get; set; } = "1";
        public TKey CreatedBy { get; set; }
        public TKey LastUpdatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
    }
}
