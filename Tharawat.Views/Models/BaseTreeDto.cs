﻿using System.ComponentModel.DataAnnotations;

namespace Tharwat.Views.Models
{
    public class BaseTreeDto : BaseTreeDto<decimal>
    {
    }  
    
    public class BaseTreeDto<TKey> : BaseDto<TKey>
    {
        public string IsMain { get; set; } = "0";

        //ATTENTION!!
        //PLEASE DO NOT REMOVE THIS ATTRIBUTE!!
        //IT IS BEING SET IN VIEW...
        [Required(ErrorMessage = "يجب إدخال هذا الحقل!")]
        public TKey? ParentId { get; set; }
    }
}